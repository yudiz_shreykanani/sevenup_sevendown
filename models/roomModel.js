const mongoose = require("mongoose");

const roomSchema = new mongoose.Schema({
  sRoomname: { type: String, required: true, unique: true },
  aUsers: [
    {
      sUsername: String,
      nBetAmount: Number,
      nBet: Number,
    },
  ],
});

const roomModel = mongoose.model("rooms", roomSchema);

module.exports = roomModel;
