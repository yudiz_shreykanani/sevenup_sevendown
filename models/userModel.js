const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  sUsername: { type: String, required: true },
  nBalance: { type: Number, default: 0 },
});

const userModel = mongoose.model("users", userSchema);

module.exports = userModel;
