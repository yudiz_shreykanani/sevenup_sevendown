const express = require("express");
const roomController = require("./../controllers/roomController");

// const app = express();
const router = express.Router();

router.route("/").post(roomController.createRoom);
router.route("/:sRoomname").put(roomController.rollDice);
module.exports = router;
