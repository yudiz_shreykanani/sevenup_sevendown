const express = require("express");
const userController = require("./../controllers/userController");

// const app = express();
const router = express.Router();

router.route("/").get(userController.getUser).post(userController.createUser);

router.route("/:sUsername").get(userController.getUserByName);
module.exports = router;
