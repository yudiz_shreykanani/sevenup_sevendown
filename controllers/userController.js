const userModel = require("./../models/userModel");

exports.createUser = (req, res) => {
  (async () => {
    try {
      userModel.findOne({ sUsername: req.body.sUsername }, (err, data) => {
        // console.log(docs);
        if (data) {
          return res.status(409).json({
            error: "User already exists !",
          });
        } else {
          const myData = new userModel({
            sUsername: req.body.sUsername,
            nBalance: req.body.nBalance,
          });
          myData
            .save()
            .then((doc) => res.json(doc))
            .catch((err) => res.json(err));
        }
      });
      //   return res.json(myData);
    } catch (error) {
      console.log(error);
    }
  })();
};

exports.getUser = (req, res) => {
  (async () => {
    try {
      userModel
        .find({})
        .then((doc) => res.json(doc))
        .catch((err) => res.json(err));
      //   return res.json(myData);
    } catch (error) {
      console.log(error);
    }
  })();
};

exports.getUserByName = (req, res) => {
  (async () => {
    try {
      userModel
        .find({ sUsername: req.params.sUsername })
        .then((doc) => res.json(doc))
        .catch((err) => res.json(err));
      //   return res.json(myData);
    } catch (error) {
      console.log(error);
    }
  })();
};
