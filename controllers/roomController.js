const roomModel = require("./../models/roomModel");
const userModel = require("./../models/userModel");

exports.createRoom = (req, res) => {
  (async () => {
    try {
      var flag = 0;
      roomModel.findOne({ sRoomname: req.body.sRoomname }, (err, docs) => {
        if (!docs) {
          userModel.find({}, (err, data) => {
            var z = 0;
            var y = 0;
            var x = 0;
            // console.log(data);
            for (let i = 0; i < req.body.aUsers.length; i++) {
              const element1 = req.body.aUsers[i];
              for (let j = 0; j < data.length; j++) {
                const element2 = data[j];
                // console.log(
                //   `${i},${j} = ${element1.sUsername},${element2.sUsername}`
                // );
                if (element1.sUsername == element2.sUsername) {
                  z = 1;
                  if (element1.nBetAmount > element2.nBalance) {
                    x = 1;
                    break;
                  }
                  break;
                }
                if (
                  element1.sUsername !== element2.sUsername &&
                  j == data.length - 1
                ) {
                  y = 1;
                  break;
                }
              }
              if (z == 0 || y == 1) {
                return res.json({
                  error: "User must be created first !",
                });
                // break;
              }
              if (x == 1) {
                return res.json({
                  error: "User does not have enough money !",
                });
                // break;
              }
            }
            if (z !== 0) {
              const myData = new roomModel({
                sRoomname: req.body.sRoomname,
                aUsers: req.body.aUsers,
              });

              myData
                .save()
                .then((data) => res.json(data))
                .catch((err) => res.json(err));
            }
          });
        } else {
          return res.json({
            error: "Room already exist !",
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  })();
};

exports.rollDice = (req, res) => {
  (async () => {
    try {
      var random_number = Math.ceil(Math.random() * 11) + 1;
      if (random_number > 7) {
        var betResult = 1;
      } else if (random_number < 7) {
        var betResult = -1;
      } else {
        var betResult = 0;
      }

      const myData = await roomModel.aggregate([
        {
          $facet: {
            winner: [
              {
                $match: {
                  sRoomname: req.params.sRoomname,
                },
              },
              {
                $project: {
                  aUsers: {
                    $filter: {
                      input: "$aUsers",
                      as: "res",
                      cond: {
                        $eq: ["$$res.nBet", betResult],
                      },
                    },
                  },
                },
              },

              {
                $replaceRoot: {
                  newRoot: {
                    sUsername: "$$CURRENT.aUsers.sUsername",
                    nBetAmount: "$$CURRENT.aUsers.nBetAmount",
                    resultOfDice: betResult,
                  },
                },
              },

              {
                $addFields: {
                  aUsers: {
                    $map: {
                      input: { $range: [0, { $size: "$sUsername" }] },
                      in: {
                        sUsername: {
                          $arrayElemAt: ["$sUsername", "$$this"],
                        },
                        amount: {
                          $arrayElemAt: ["$nBetAmount", "$$this"],
                        },
                      },
                    },
                  },
                },
              },

              {
                $project: {
                  aUsers: 1,
                  resultOfDice: 1,
                  winningAmounts: {
                    $map: {
                      input: "$aUsers.amount",
                      as: "res",
                      in: {
                        $cond: {
                          if: { $eq: ["$resultOfDice", 0] },
                          then: {
                            // "aUsers.sUsername": "$$res.sUsername",
                            $multiply: ["$$res", 4],
                          },
                          else: {
                            // "aUsers.sUsername": "$$res.sUsername",

                            $multiply: ["$$res", 2],
                          },
                        },
                      },
                    },
                  },
                  // aUsers: 1,
                },
              },

              {
                $lookup: {
                  from: "users",
                  localField: "aUsers.sUsername",
                  foreignField: "sUsername",
                  as: "usertabledata",
                },
              },

              {
                $project: {
                  resultOfDice: 1,
                  aUsers: {
                    $map: {
                      input: "$aUsers",
                      as: "one",
                      in: {
                        $mergeObjects: [
                          "$$one",
                          {
                            $arrayElemAt: [
                              {
                                $filter: {
                                  input: "$usertabledata",
                                  as: "two",
                                  cond: {
                                    $eq: ["$$two.sUsername", "$$one.sUsername"],
                                  },
                                },
                              },
                              0,
                            ],
                          },
                        ],
                      },
                    },
                  },
                },
              },
              {
                $project: {
                  resultOfDice: 1,
                  aUsers: 1,
                  updatedBalance: {
                    $map: {
                      input: "$aUsers",
                      as: "res",
                      in: {
                        sUsername: "$$res.sUsername",
                        nBalance: {
                          $cond: {
                            if: { $eq: [betResult, 0] },
                            then: {
                              $add: [
                                { $multiply: ["$$res.amount", 4] },
                                "$$res.nBalance",
                              ],
                            },
                            else: {
                              $add: [
                                { $multiply: ["$$res.amount", 2] },
                                "$$res.nBalance",
                              ],
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
              {
                $project: {
                  aUsers: 0,
                },
              },
            ],

            /////////////////////////////////////////////////////////////////

            loosers: [
              {
                $match: {
                  sRoomname: req.params.sRoomname,
                },
              },
              {
                $project: {
                  aUsers: {
                    $filter: {
                      input: "$aUsers",
                      as: "res",
                      cond: {
                        $ne: ["$$res.nBet", betResult],
                      },
                    },
                  },
                },
              },
              {
                $replaceRoot: {
                  newRoot: {
                    sUsername: "$$CURRENT.aUsers.sUsername",
                    nBetAmount: "$$CURRENT.aUsers.nBetAmount",
                    resultOfDice: betResult,
                  },
                },
              },

              {
                $addFields: {
                  aUsers: {
                    $map: {
                      input: { $range: [0, { $size: "$sUsername" }] },
                      in: {
                        sUsername: {
                          $arrayElemAt: ["$sUsername", "$$this"],
                        },
                        amount: {
                          $arrayElemAt: ["$nBetAmount", "$$this"],
                        },
                      },
                    },
                  },
                },
              },

              {
                $lookup: {
                  from: "users",
                  localField: "aUsers.sUsername",
                  foreignField: "sUsername",
                  as: "usertabledata",
                },
              },

              {
                $project: {
                  resultOfDice: 1,
                  aUsers: {
                    $map: {
                      input: "$aUsers",
                      as: "one",
                      in: {
                        $mergeObjects: [
                          "$$one",
                          {
                            $arrayElemAt: [
                              {
                                $filter: {
                                  input: "$usertabledata",
                                  as: "two",
                                  cond: {
                                    $eq: ["$$two.sUsername", "$$one.sUsername"],
                                  },
                                },
                              },
                              0,
                            ],
                          },
                        ],
                      },
                    },
                  },
                },
              },
              {
                $project: {
                  resultOfDice: 1,
                  aUsers: 1,
                  updatedBalance: {
                    $map: {
                      input: "$aUsers",
                      as: "res",
                      in: {
                        sUsername: "$$res.sUsername",
                        nBalance: {
                          $subtract: [
                            // { $subtract: ["$$res.nBalance", "$$res.amount"] },
                            "$$res.nBalance",
                            "$$res.amount",
                          ],
                        },
                      },
                    },
                  },
                },
              },
              {
                $project: {
                  aUsers: 0,
                },
              },
            ],
          },
        },

        { $project: { finaldata: { $setUnion: ["$winner", "$loosers"] } } },
        { $unwind: "$finaldata" },
        { $unwind: "$finaldata.updatedBalance" },
        {
          $project: {
            "finaldata.resultOfDice": 0,
          },
        },
        {
          $project: {
            updatedData: "$finaldata.updatedBalance",
          },
        },
        {
          $project: {
            sUsername: "$updatedData.sUsername",
            nBalance: "$updatedData.nBalance",
          },
        },
        {
          $merge: {
            into: "users",

            on: "sUsername", // Optional

            whenMatched: "replace", // Optional
            whenNotMatched: "discard", // Optional
          },
        },

        ///////////////////////////////////
      ]);

      return res.json(myData);
      //   res.json({ number: random_number });
    } catch (error) {
      console.log(error);
    }
  })();
};
